﻿
using UnityEngine;

public class Player : MonoBehaviour {

    public Rigidbody rig;
    public float ForwardForce = 2000f;
    public float SideForce = 500f;
	void Start () 
    {
        
	}   
	
	// Update is called once per frame
	void FixedUpdate () 
    {
 
            rig.AddForce(0, 0, ForwardForce * Time.deltaTime);

        

        if(Input.GetKey("d"))
        {
            rig.AddForce(SideForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }
        
        if (Input.GetKey("a"))
        {
            rig.AddForce(-SideForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if(rig.position.y < -2f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
	}
}
