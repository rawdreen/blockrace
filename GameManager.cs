﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

     bool gameHasEnded = false;

    public float restartDelay = 2f;

    public GameObject completeLevelUI;
    public GameObject lvlfailed;

    public void CompleteLvl()
    {
        completeLevelUI.SetActive(true);
    }

   public void EndGame()
    {
       if(gameHasEnded==false)
       {
           gameHasEnded = true;
           if (SceneManager.GetActiveScene().buildIndex == 1) { Invoke("Restart", restartDelay); }
           else {lvlfailed.SetActive(true);}
       }
    }

  
    void Restart()
   {
       SceneManager.LoadScene(SceneManager.GetActiveScene().name);
   }
}
