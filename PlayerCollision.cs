﻿using UnityEngine.Audio;
using UnityEngine;

public class PlayerCollision : MonoBehaviour 
{
    public Player pl;
    public AudioSource collide;
    public AudioSource bg;

    void Start()
    {
        collide = GetComponent<AudioSource>();
    }

    void OnCollisionEnter (Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Collision")
        {
            collide.Play();
            pl.enabled = false;
            FindObjectOfType<GameManager>().EndGame();
            s();
        }
	}
    void s()
    {
        bg.pitch = bg.pitch / 2; 
    }
}
