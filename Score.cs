﻿using UnityEngine.UI;
using UnityEngine;

public class Score : MonoBehaviour {

    public Transform player;
    public Text ScoreText;
    public Transform end;

    // Update is called once per frame
	void Update () {

        if ((end.position.z - player.position.z) < 0) { ScoreText.text = " YAY! "; }
        else { ScoreText.text = (end.position.z - player.position.z ).ToString("0");  }
        
        
	}
}
