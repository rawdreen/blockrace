﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelComplete : MonoBehaviour {

	public void LoadNextLvl()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
}
